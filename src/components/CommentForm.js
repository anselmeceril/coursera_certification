import React, { Component } from 'react';
import {Label, Input, Col, Row, Modal, ModalBody, ModalHeader, ModalFooter, Button} from 'reactstrap';
import { Control, Form, LocalForm, Errors, actions } from 'react-redux-form';



class CommentForm extends Component {

    constructor(props) {
        super(props);


        this.state = {
            firstname: '', 
            rating: '1', 
            comment: '',
        }

        this.handleSubmit = this.handleSubmit.bind(this);

    }
    
    handleSubmit(values) {
        console.log('Current State is: ' + JSON.stringify(values));
        alert('Current State is: ' + JSON.stringify(values));
        this.props.resetFeedbackForm();
        // event.preventDefault();
    }

    

    render() {

        const required = (val) => val && val.length;
        const maxLength = (len) => (val) => !(val) || (val.length <= len);
        const minLength = (len) => (val) => val && (val.length >= len);
        const isNumber = (val) => !isNaN(Number(val));
        const validEmail = (val) => /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(val);

        return (
            <div>
                <Modal isOpen={this.props.isOpen}>

                    <ModalHeader>Submit Comment</ModalHeader>

                    <ModalBody>

                        <Form model="feedback" onSubmit={(values) => this.handleSubmit(values)}>                         
                            <Row className="form-group">
                                <Label htmlFor="rating" md={6}>Rating</Label>
                                <Col md={18}>
                                    <Control.select 
                                            model=".rating" 
                                            id="rating" 
                                            name="rating"  
                                            placeholder="rating"
                                    >
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </Control.select>
                                </Col>
                            </Row>

                            <Row className="form-group">
                                <Label htmlFor="firstname" md={3}>Your Name</Label>
                                <Col md={18}>
                                    <Control.text model=".firstname" id="firstname" name="firstname"
                                            placeholder="First Name"
                                            className="form-control"
                                            validators={{
                                                required, minLength: minLength(3), maxLength: maxLength(15)
                                        }}
                                    />
                                    <Errors
                                        className="text-danger"
                                        model=".firstname"
                                        show="touched"
                                        messages={{
                                            required: 'Required',
                                            minLength: 'Must be greater than 2 characters',
                                            maxLength: 'Must be 15 characters or less'
                                        }}
                                    />
                                </Col>
                            </Row>

                            <Row className="form-group">
                                <Label htmlFor="comment" md={2}>Comment</Label>
                                <Col md={10}>
                                    <Control.textarea model=".comment" id="comment" name="comment"
                                            placeholder="comment"
                                            className="form-control"
                                            validators={{
                                                required, minLength: minLength(5), maxLength: maxLength(15)
                                        }}
                                    />
                                    <Errors
                                        className="text-danger"
                                        model=".comment"
                                        show="touched"
                                        messages={{
                                            required: 'Required',
                                            minLength: 'Must be greater than 2 characters',
                                            maxLength: 'Must be 15 characters or less'
                                        }}
                                    />
                                </Col>
                            </Row>

                            <ModalFooter>
                                <Button color="primary" size="lg" type="submit" > Submit </Button>{' '}
                            </ModalFooter>
                        </Form>    
                    </ModalBody>
                    
                    
                </Modal>
            </div>
        )
    }
}

export default CommentForm;