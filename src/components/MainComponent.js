// Import react package
import React, { Component } from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';

// Import boostrap and react strap 
import { Navbar, NavbarBrand } from 'reactstrap';

// Import data
import { DISHES } from "../shared/dishes";
import { COMMENTS } from '../shared/comments';
import { PROMOTIONS } from '../shared/promotions';
import { LEADERS } from '../shared/leaders';

// Import Component 
import Menu from "./MenuComponent";
import DishDetail from "./DishdetailComponent";
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import Home from './HomeComponent';
import Contact from "./ContactComponent";
import About from "./AboutComponent";

import { connect } from 'react-redux';  

import { addComment, fetchDishes, fetchComments, fetchPromos, postComment, fetchLeaders } from '../redux/ActionCreators';

import { actions } from 'react-redux-form';




const mapStateToProps = state => {
    return {
      dishes: state.dishes,
      comments: state.comments,
      promotions: state.promotions,
      leaders: state.leaders
    }
}

const mapDispatchToProps = dispatch => ({
    addComment: (dishId, rating, author, comment) => dispatch(addComment(dishId, rating, author, comment)),
    fetchDishes: () => { dispatch(fetchDishes())},
    resetFeedbackForm: () => { dispatch(actions.reset('feedback'))},
    fetchComments: () => dispatch(fetchComments()),
    fetchPromos: () => dispatch(fetchPromos()), 
    postComment: (dishId, rating, author, comment) => dispatch(postComment(dishId, rating, author, comment)), 
    fetchLeaders: () => dispatch(fetchLeaders)
});



  


class MainComponent extends Component {

    


    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.fetchDishes();
        this.props.fetchComments();
        this.props.fetchPromos();
        this.props.fetchLeaders();
    }

    onDishSelect(dishId) {
        this.setState({selectedDish: dishId})
    }


  

    render() {

        const HomePage = () => {
            return(
                <Home 
                    dish={this.props.dishes.dishes.filter((dish) => dish.featured)[0]}
                    dishesLoading={this.props.dishes.isLoading}
                    dishErrMess={this.props.dishes.errMess}
                    promotion={this.props.promotions.promotions.filter((promo) => promo.featured)[0]}
                    promoLoading={this.props.promotions.isLoading}
                    promoErrMess={this.props.promotions.errMess}
                    leaders={this.props.leaders.leaders.filter((leader) => leader.featured)[0]}
                    postComment={this.props.postComment}
                />
            );
          }
        
        
        const DishWithId = ({match}) => {
            if (this.state.dishes && this.state.comments){
                console.log("COMMMENT ON MAIN ==> ", this.state.comments.filter((comment) => comment.dishId === parseInt(match.params.dishId, 10)))

                return (
                    
                    <DishDetail dish={this.props.dishes.dishes.filter((dish) => dish.id === parseInt(match.params.dishId,10))[0]}
                        isLoading={this.props.dishes.isLoading}
                        errMess={this.props.dishes.errMess}
                        comments={this.props.comments.comments.filter((comment) => comment.dishId === parseInt(match.params.dishId,10))}
                        commentsErrMess={this.props.comments.errMess}
                        addComment={this.props.addComment}
                    />

                )
            }
            
            else return(<div></div>)
        }
          
        return (
            <div>
                
                <Header></Header>
                <Switch>
                    <Route path="/home" component={HomePage} /> 
                    <Route path="/aboutus" component={() => <About leaders={this.state.leaders}/>}/>                                                  />
                    <Route exact path="/menu" component={() => <Menu dishes={this.state.dishes}/>} />
                    <Route path="/menu/:dishId" component={DishWithId}></Route>
                    <Route exact path='/contactus' component={() => <Contact resetFeedbackForm={this.props.resetFeedbackForm} />} />
                    <Redirect to="/home" />
                </Switch>
                <Footer></Footer>



                

                {/* <Menu dishes={this.state.dishes} onClick={(dishId) => this.onDishSelect(dishId)}></Menu>
                
                <DishDetail dish={this.state.dishes.filter((dish) => dish.id === this.state.selectedDish)[0]}></DishDetail> */}

            </div>
        )
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MainComponent));