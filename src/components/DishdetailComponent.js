import React, { Component } from 'react';
import { Card, CardImg, CardText, CardBody, CardTitle, Breadcrumb, BreadcrumbItem, Button, Modal, ModalHeader, ModalBody, Label, Col, Row} from 'reactstrap';
import { Link } from 'react-router-dom';
import { Control, LocalForm, Errors } from 'react-redux-form';


const required = (val) => val && val.length;
const minLength = (len) => (val) => (val) && (val.length >= len);
const maxLength = (len) => (val) => !(val) || (val.length <= len);


class CommentForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isModalOpen: false
    }

    this.toggleModal = this.toggleModal.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  toggleModal() {
    this.setState({ isModalOpen : !this.state.isModalOpen });
  }

  handleSubmit(values) {
    this.toggleModal();
    this.props.postComment(this.props.dishId, values.rating, values.author, values.comment);
    console.log("Current state is: " + JSON.stringify(values));
    alert("Current state is: " + JSON.stringify(values));
  }

  render() {
    return(
      <div>
        <Button outline onClick={this.toggleModal}><span className="fa fa-edit fa-lg"></span> Leave a comment</Button>
        <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
          <ModalHeader toggle={this.toggleModal}>Your comment</ModalHeader>
          <ModalBody>
            <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
              <Row className="form-group">
                <Label htmlFor="rating" md={2}>Rating</Label>
                <Col md={10}>
                  <Control.select model=".rating" name="rating" className="form-control">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                  </Control.select>
                </Col>
              </Row>
              <Row className="form-group">
                <Label htmlFor="author" md={2}>Author</Label>
                <Col md={10}>
                  <Control.text model=".author" id="author" name="author" 
                    placeholder="Author"
                    className="form-control"
                    validators = {{
                      required, minLength: minLength(3), maxLength: maxLength(15)
                    }} />
                    <Errors 
                      className="text-danger"
                      model=".author"
                      show="touched"
                      messages={{
                        required: 'Required',
                        minLength: 'Must be greater than 2 characters',
                        maxLength: 'Must be 15 characters or less'
                      }} />
                </Col>
              </Row>
              <Row className="form-group">
                <Label htmlFor="message" md={12}>Leave a message</Label>
                <Col md={12}>
                  <Control.textarea model=".message" id="message" name="message" rows="6" className="form-control" />
                </Col>
              </Row>
              <Row className="form-group">
                <Col md={12}>
                  <Button type="submit" color="primary">Submit comment</Button>
                </Col>
              </Row>
            </LocalForm>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}


function RenderDish({dish}) {
  return(
    <div  className="col-12 col-md-5 m-1">
      <Card>
        <CardImg top src={dish.image} alt={dish.name} />
        <CardBody>
          <CardTitle>{dish.name}</CardTitle>
          <CardText>{dish.description}</CardText>
        </CardBody>
      </Card>
    </div>
  );
}


function RenderComments({comments, postComment}) {
  if( comments !== null ) {
    
    const commentList = comments.map((comment) => {
      // quick & dirty formatter (to string)
      const d = new Date(comment.date);
      const dString = new Intl.DateTimeFormat('en', { month: 'long' }).format(d) +","+ 
                      new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d) +","+ 
                      new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);

      return (            
        <li key="{comment.id}">
          <p>{comment.comment}</p>
          <p>-- {comment.author}, {dString}</p>
        </li>
      );
    });

    return(
      <div  className="col-12 col-md-5 m-1">
        <div>
            <h4>Comments</h4>
            <ul className="list-unstyled">
                {commentList}
            </ul>
        </div>
        <div>
          <CommentForm  postComment={postComment}/>
        </div>        
      </div>
    );

  } else {

      return(
        <div>
          <CommentForm />
        </div>
      );

  }  
}


const Dishdetail = (props) => {
  if( props.dish !== null && typeof props.dish !== 'undefined' ) {

    return(
      <div className="container">
        <div className="row">
          <Breadcrumb>
            <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
            <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
          </Breadcrumb>
          <div className="col-12">
            <h3>{props.dish.name}</h3>
            <hr/>
          </div>
        </div>
        <div className="row">
          <RenderDish dish={props.dish} />
          <RenderComments comments={props.comments} />
        </div>
      </div>
    );

  } else {

    return(
      <div></div>
    );

  }
}


export default Dishdetail;


// import React, { Component, useState } from 'react'
// import { Card, CardImg, CardImgOverlay, CardText, CardBody, Button,
//     CardTitle, BreadcrumbItem, Container, Row, Col, Modal, ModalBody, ModalHeader, ModalFooter, Label, Input  } from 'reactstrap';
// import {Link} from 'react-router-dom';
// import { Control, LocalForm, Errors } from 'react-redux-form';
// import {Loading} from "./LoadingComponent";
// import { baseUrl } from '../shared/baseUrl';





// class CommentForm extends Component {

//     constructor(props) {
//         super(props);
    
//         this.state = {
//           isModalOpen: false
//         }
    
//         this.toggleModal = this.toggleModal.bind(this);
//         this.handleSubmit = this.handleSubmit.bind(this);
//       }
    
//       toggleModal() {
//         this.setState({ isModalOpen : !this.state.isModalOpen });
//       }
    
//       handleSubmit(values) {
//         this.toggleModal();
//         this.props.postComment(this.props.dishId, values.rating, values.author, values.comment);
//         console.log("Current state is: " + JSON.stringify(values));
//         alert("Current state is: " + JSON.stringify(values));
//       }

    

//     render() {

//         const required = (val) => val && val.length;
//         const maxLength = (len) => (val) => !(val) || (val.length <= len);
//         const minLength = (len) => (val) => val && (val.length >= len);
      
//         return (
//             <div>
//                 <Modal isOpen={this.props.isOpen}>

//                     <ModalHeader>Submit Comment</ModalHeader>

//                     <ModalBody>

//                         <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
                         
//                             <Row className="form-group">
//                                 <Label htmlFor="rating" md={6}>Rating</Label>
//                                 <Col md={18}>
//                                     <Control.select 
//                                             model=".rating" 
//                                             id="rating" 
//                                             name="rating"  
//                                             placeholder="rating"
//                                     >
//                                         <option value="1">1</option>
//                                         <option value="2">2</option>
//                                         <option value="3">3</option>
//                                     </Control.select>
//                                 </Col>
//                             </Row>

//                             <Row className="form-group">
//                                 <Label htmlFor="firstname" md={3}>Your Name</Label>
//                                 <Col md={18}>
//                                     <Control.text model=".firstname" id="firstname" name="firstname"
//                                             placeholder="First Name"
//                                             className="form-control"
//                                             validators={{
//                                                 required, minLength: minLength(3), maxLength: maxLength(15)
//                                         }}
//                                     />
//                                     <Errors
//                                         className="text-danger"
//                                         model=".firstname"
//                                         show="touched"
//                                         messages={{
//                                             required: 'Required',
//                                             minLength: 'Must be greater than 2 characters',
//                                             maxLength: 'Must be 15 characters or less'
//                                         }}
//                                     />
//                                 </Col>
//                             </Row>

//                             <Row className="form-group">
//                                 <Label htmlFor="comment" md={2}>Comment</Label>
//                                 <Col md={10}>
//                                     <Control.textarea model=".comment" id="comment" name="comment"
//                                             placeholder="comment"
//                                             className="form-control"
//                                             validators={{
//                                                 required, minLength: minLength(5), maxLength: maxLength(15)
//                                         }}
//                                     />
//                                     <Errors
//                                         className="text-danger"
//                                         model=".comment"
//                                         show="touched"
//                                         messages={{
//                                             required: 'Required',
//                                             minLength: 'Must be greater than 2 characters',
//                                             maxLength: 'Must be 15 characters or less'
//                                         }}
//                                     />
//                                 </Col>
//                             </Row>
//                             <ModalFooter>
//                                 <Button color="primary" size="lg" type="submit" > Submit </Button>
//                             </ModalFooter>
//                         </LocalForm>    
//                     </ModalBody>
//                 </Modal>
//             </div>
//         )
//     }
// }


// function RenderDish({dish}) {
//     if (dish){
//         return(
           
//             <Card>
//                 <CardImg top src={baseUrl + dish.image} alt={dish.name} />
//                     <CardBody>
//                     <CardTitle>{dish.name}</CardTitle>
//                     <CardText>{dish.description}</CardText>
//                 </CardBody>
//             </Card> 
//         )
//     }
//     else {
//         return(
//             <div></div>
//         )
//     }
// }

// function RenderComments({comments, postComment, dishId}) {
//     if (comments && comments.length > 0){
//         return comments.map((comment) => {
//             const date = new Date(comment.date).toDateString();
//             if (comment){
//                 return(
//                     <div key={comment.id}>
//                         <ul className = "list-unstyled">
//                             <li>
//                                 {comment.comment} 
//                                 <br/>
//                                 -- {comment.author}, {date}
//                             </li>
//                         </ul>
//                     </div>

//                 )
//             }
//             else
//                 return(
//                     <div></div>
//                 );   
//        })
//     }
//     else {
//         return(<div></div>);
       
//     }
// }


// const  DishDetail = (props) => {

//     const [isModelOpen, setIsModelOpen, dishId] = useState(false)



//     if (props.isLoading) {
//         return(
//             <div className="container">
//                 <div className="row">            
//                     <Loading />
//                 </div>
//             </div>
//         );
//     }
//     else if (props.errMess) {
//         return(
//             <div className="container">
//                 <div className="row">            
//                     <h4>{props.errMess}</h4>
//                 </div>
//             </div>
//         );
//     }
//     else if (props.dish != null) {

//         return (
//             <>
//                 <CommentForm 
//                     isOpen={isModelOpen} 
//                     onClose={() => setIsModelOpen(false)} 
//                     dishId={dishId}
//                     postComment={postComment}
//                 />
//                 <Container>

//                     <Row>
//                         <BreadcrumbItem>
//                             <Link to='/menu'>Menu</Link>
//                         </BreadcrumbItem>

//                         <BreadcrumbItem active>
//                             {props.dish.name}
//                         </BreadcrumbItem>
//                     </Row>

//                     <div className="col-12">
//                         <h3>{props.dish.name}</h3>

//                     </div>

//                     <Row>
//                         <Col xs="6"> 
//                             <RenderDish dish={props.dish}/> 
//                         </Col>
//                         <Col xs="6">
//                             <RenderComments 
//                                 comments={props.comments} 
//                                 postComment={props.postComment}
//                                 dishId={props.dish.id}
//                             />
//                             <Button color="secondary" size="lg" onClick={() => setIsModelOpen(true)}> Submit Comment </Button>{' '}
//                         </Col>
//                     </Row>
//                 </Container>
//             </>
            
            

            
//         )
//     }
//     else
//         return (
//             <div></div>
//         );
// }

// export default DishDetail;