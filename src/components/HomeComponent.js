import React, { Component } from 'react'
import { Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle} from 'reactstrap';
import {Loading} from './LoadingComponent'
import { baseUrl } from '../shared/baseUrl';



function RenderCard ({item, isLoading, errMess}) {

        
        if (isLoading && item) {
            return(
                <Loading />
            );
        }
        else if (errMess) {
            return(
                    <h4>{errMess}</h4>
            );
        }
        else 
            return(
                
                <Card>
                    {console.log("itemmmmmmmmmmmmmmmmm----> : ", isLoading, errMess,  item)}
                    
                    <CardImg src={baseUrl + item.image} alt={baseUrl + item.name} />
                    <CardBody>
                        <CardTitle>{item.name}</CardTitle>
                        {item.designation ? <CardSubtitle>{item.designation}</CardSubtitle> : null }
                        <CardText>{item.description}</CardText>
                    </CardBody>
                </Card>
            );
}

function Home(props) {
    return (
        <div className="container">
            {console.log(props)}
            <div className="row align-items-start">
                <div className="col-12 col-md m-1">
                    <RenderCard item={props.dish} isLoading={props.dishesLoading} errMess={props.dishesErrMess}  />
                </div>
                <div className="col-12 col-md m-1">
                    <RenderCard item={props.promotion} isLoading={props.promoLoading} errMess={props.promoErrMess} />

                </div>
                <div className="col-12 col-md m-1">
                    {console.log("LEADERS ===> ", props)}
                    <RenderCard item={props.leaders} isLoading={props.dishesLoading} errMess={props.dishesErrMess}  />
                </div>
            </div>
        </div>

    )
}

export default Home;