import { LEADERS } from '../shared/leaders';
import * as ActionTypes from './ActionTypes';


const initialState = {
    pending: false,
    leaders: [],
    error: null
}



export const Leaders = (state = initialState, action) => {
    switch (action.type) {

        case ActionTypes.FETCH_LEADERS_PENDING:
            return {
                ...state, 
                pending: true
            }
        
        case ActionTypes.FETCH_LEADERS_SUCCESS: 
            return {
                ...state, 
                pending: false,
                leaders: action.payload 
            }


        case ActionTypes.FETCH_LEADERS_ERROR:
            return {
                ...state, 
                pending: false, 
                error: action.payload
            }

        default:
          return state;
      }
};