import * as ActionTypes from './ActionTypes';
import { DISHES } from '../shared/dishes';
import { baseUrl } from '../shared/baseUrl';
import { Promotions } from './promotions';




// ---------------------- Action to Add Comments ------------------------------

export const addComment = (dishId, rating, author, comment) => ({
    type: ActionTypes.ADD_COMMENT,
    payload: {
        dishId: dishId,
        rating: rating,
        author: author,
        comment: comment
    }
});


export const addComments = (comments) => ({
    type: ActionTypes.ADD_COMMENTS, 
    payload: comments
})

export const fetchComments = () => (dispatch) => {
    return fetch(baseUrl + 'comments')
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            var errmess = new Error(error.message);
            throw errmess;
      })
    .then(response => response.json())
    .then(comments => dispatch(addComments(comments)))
    .catch(error => dispatch(commentsFailed(error.message)));
}

export const postComment = (dishId, rating, author, comment) => (dispatch) => {

    const newComment = {
        dishId: dishId,
        rating: rating,
        author: author,
        comment: comment
    };
    newComment.date = new Date().toISOString();
    
    return fetch(baseUrl + 'comments', {
        method: "POST",
        body: JSON.stringify(newComment),
        headers: {
          "Content-Type": "application/json"
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(response => dispatch(addComment(response)))
    .catch(error =>  { console.log('post comments', error.message); alert('Your comment could not be posted\nError: '+error.message); });
};


// ---------------------- Action to Add Dishes ---------------------------------

export const addDishes = (dishes) => ({
    type: ActionTypes.ADD_DISHES, 
    payload: dishes
})

export const fetchDishes = () => (dispatch) => {


    dispatch(dishesLoading(true));

    return fetch(baseUrl + 'dishes')
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            var errmess = new Error(error.message);
            throw errmess;
    })
    .then(response => response.json())
    .then(dishes => dispatch(addDishes(dishes)))
    .catch(error => dispatch(dishesFailed(error.message)));
}

//--------------------- Case Loading or message error --------------------------- 

export const dishesLoading = () => ({
    type: ActionTypes.DISHES_LOADING
})

export const promosLoading = () => ({
    type: ActionTypes.PROMOS_LOADING
});

export const promosFailed = (errmess) => ({
    type: ActionTypes.PROMOS_FAILED,
    payload: errmess
});
export const dishesFailed = (errmess) => ({
    type: ActionTypes.DISHES_FAILED, 
    payload: errmess
})

export const commentsFailed = (errmess) => ({
    type: ActionTypes.COMMENTS_FAILED, 
    payload: errmess
})
// ---------------------- Action to Add Promos ---------------------------------


export const addPromos = (promotions) => ({
    type: ActionTypes.ADD_PROMOS, 
    payload: promotions
})

export const fetchPromos = () => (dispatch) => {

    dispatch(promosLoading());

    return fetch(baseUrl + 'promotions')
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            var errmess = new Error(error.message);
            throw errmess;
      })
    .then(response => response.json())
    .then(promos => dispatch(addPromos(promos)))
    .catch(error => dispatch(promosFailed(error.message)));
}

// ------------------------- Action to fetch Leaders --------------------------------------

export const fetchLeadersPending = () => ({
  type: ActionTypes.FETCH_LEADERS_PENDING
})

export const fetchLeadersSuccess = (leaders) => ({
  type: ActionTypes.FETCH_LEADERS_SUCCESS,
  payload : leaders
})

export const fetchLeadersError = (error) => ({
  type: ActionTypes.FETCH_LEADERS_ERROR, 
  payload : error
})


export const fetchLeaders = (dispatch) => {
  
  dispatch(fetchLeadersPending());

  fetch(baseUrl + 'leaders')
  .then(response => {
    if (response.ok) {
      return response;
    } else {
        var error = new Error('Error ' + response.status + ': ' + response.statusText);
        error.response = response;
        throw error;
    }
  }, 
  error => {
    var errmess = new Error(error.message);
    throw errmess;
  })
  .then(response => response.json())
  .then(leaders => dispatch(fetchLeadersSuccess(leaders)))
  .catch(error => dispatch(fetchLeadersError(error)));

}